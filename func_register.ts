import { player_table} from "./main_listen";
import net from 'net'


export function register_account(inputstr:string,socket:net.Socket,Custor:{[key:string]:any}){
    if (inputstr in player_table)
    {
        socket.write('已成功登陆，账号为'+inputstr);
    }
    else 
    {
        socket.write('已成功注册，账号为'+inputstr);
    }
    player_table[inputstr]={
        name:inputstr,
        online:true
    };
    Custor.player_name=inputstr;
    Custor.register=false;
}