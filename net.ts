//引入net模块
import readline from 'readline';
import net from 'net';

// 一些全局变量
//let player_name:string=''; // 当前客户端玩家姓名
// 此句仅用于测试fork
//创建TCP客户端

const client = new net.Socket();

//设置连接的服务器

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

client.connect(8001, '127.0.0.1',  () => {
    console.log("连接服务器成功");
    
    // ask user for the anme input
    rl.question(`请输入你的账号:`, (name) => {
      client.write(name);
    });
});

//监听data事件

client.on("data",  (data) => {
  // 仅用于判断服务器连接状态，不断交替发送信号1
  if (data.toString()!='1') 
  {
    console.log(data.toString());
    return;
  }

  rl.question(`请输入你的指令:`, (str) => {
      client.write(str);
      if (str=='leave')
      {
        console.log('已发送登出指令');
        client.end();
      }
      else if (str=='create room' || str=='choose room' || str=='leave room')
      {
        console.log('已发送房间指令');
      }
      else 
      {
        console.log('无效指令'); 
      }
  });

});


