
import { room_table,player_table,Room,order_judge} from "./main_listen";
import net from 'net'

// 房间选择函数
export function Room_choose(inputstr:string,socket:net.Socket,Custor:{[key:string]:any}){
    new Promise((resolve, reject) => {
        if  (room_table[inputstr] != undefined)
        {
            resolve("1");
        }
        else reject("不存在该房间")
    }).then(
        value => {
            if  (player_table[Custor.player_name].room != undefined)
            {
                return Promise.reject('已有房间');
            }
        },
        reason => {
            return Promise.reject(reason);
        }
    ).then(
        value => {
            room_table[inputstr].number++;
            room_table[inputstr].player[Custor.player_name]=socket;
            player_table[Custor.player_name].room=inputstr;
            socket.write('成功加入房间');
        },
        reason => {
            socket.write(reason);
        }
    );
    Custor.choose=false;
    Custor.mit='1';
}

// 指令函数

export const create_room=(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>{
    if (player_table[Custor.player_name].room == undefined)
    {
        room_table[Room.cnt.toString()]=new Room(1,Custor.player_name,true,{},[]);
        player_table[Custor.player_name].room=Room.cnt.toString();
        room_table[Room.cnt.toString()].player[Custor.player_name]=socket;
        Room.cnt++;
        socket.write("房间创建成功，房主为"+Custor.player_name);
    }
    else
    {
        socket.write("创建失败，已加入房间"+player_table[Custor.player_name].room);
    }
}

export const choose_room=(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>{
    socket.write("可选房间为：\n")
    for (const key in room_table) {
        socket.write("房间号"+key+" 房间人数为"+room_table[key].number);
        let str:string='';
        for (const key2 in room_table[key].player) {
            str+=key2;
            if (player_table[key2].online) str+="(在线) "
            else str+="(离线) "
        }
        socket.write("房间玩家为:"+str+'\n');
    }
    Custor.choose=true;
    Custor.mit="2";
}

export const leave_room=(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>{
    let roomid=player_table[Custor.player_name].room ; // 玩家所在房间编号
    if (roomid !== undefined)
    {
        delete room_table[roomid].player[Custor.player_name]; // 删除房间内玩家
        room_table[roomid].number--;
        delete player_table[Custor.player_name].room; // 删除玩家房间归属
        
    }
    else 
    {
        socket.write("并没有加入房间");
    }
}


