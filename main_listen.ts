
//引入net模块

import net, { SocketAddress } from 'net'
import {register_account} from "./func_register"
import {say_context,reply_context} from "./func_communicate"
import {create_room,choose_room,leave_room,Room_choose} from "./func_room"
import {game_roll}from "./func_game"



//创建TCP服务器

// 创建玩家表、房间表
interface Player{
    name:string;
    online:boolean;
    game?:boolean;
    room?:string;
}
export let player_table:{[key:string]:Player} ={};

export let room_table:{[key:string]:Room}={};
export class Room{
    static cnt:number=1;// 房间从1开始编号,使用的时候转化为string
    number:number; // 人数
    admin:string;  // 管理员
    active:boolean;  // 是否活跃
    player:{[key:string]:net.Socket};  // 具体玩家的服务器
    chat:string[]; // 聊天信息
    game:number=0;  // 房间是否处于游戏状态,0代表未开始，1代表准备中，2代表进行中
    constructor(num:number,admin:string,active:boolean,player:{[key:string]:net.Socket},
        chat:string[]){
        this.number=num;
        this.admin=admin;
        this.active=active;
        this.player=player;
        this.chat=chat;
    }
}

// 创建指令判断字典
export let order_judge:{[key:string]:(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>void} ={};


order_judge['create room']=create_room;
order_judge['choose room']=choose_room;
order_judge['leave room']=leave_room;
order_judge['say ']=say_context;
order_judge['reply ']=reply_context;
order_judge['roll']=game_roll;


const server = net.createServer( (socket)=> {
    //  连接成功，要求玩家输入注册名字
    console.log("新玩家接入")
    let Custor:{[key:string]:any}={};
    Custor['mit']='1';   // 发送至客户端的信号，1表示正常，2表示选择房间阶段
    Custor['register']=true;  // 判断当前是否是注册账号阶段
    Custor['choose']=false;    // 判断当前是否是选择房间阶段
    Custor['player_name']='';  // 记录本客户端所对应的玩家姓名

    //获取玩家输入的各种信息
    socket.on("data", (data)=> {
        let inputstr=data.toString();
        if (Custor.register)   // 注册阶段
        {
            register_account(inputstr,socket,Custor);
        } 
        else if (Custor.choose)   // 选择房间阶段（测试promise写法）
        {
            Room_choose(inputstr,socket,Custor);
        }
        else  if (inputstr!='logout') // 接收指令阶段
        {
            if (inputstr.slice(0,4)=="say " ) inputstr=inputstr.slice(0,4)
            if (inputstr.slice(0,6)=="reply ") inputstr=inputstr.slice(0,6)   // 处理字符串以满足聊天指令要求
            order_judge[inputstr](data,socket,Custor);
        }
        console.log("接收到数据为：" + data.toString());
        if (inputstr!='logout') {
            setTimeout(() => {
                socket.write(Custor.mit);
            }, 50);      // 偷个懒，房间选择新练习了promise，导致了一些延迟问题
        }
    });

    socket.on("end", ()=> {
        const readSize = socket.bytesRead;
        //打印数据
        player_table[Custor.player_name].online=false;
        console.log("玩家"+Custor.player_name+"客户端已断线" );

    });

});

//设置监听端口

server.listen( 8001, () =>{

    console.log("服务正在监听中。。。")

});

