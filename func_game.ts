import { player_table,room_table} from "./main_listen";
import net from 'net'
import { randomInt } from "crypto";
import { resolve } from "path";

async function game_ready(num:number,roomid:string){
    for (let index=num;index>0;index--)
    {
        await new Promise(
            resolve => {setTimeout(()=>{
                        for (const key in  room_table[roomid].player) {
                            room_table[roomid].player[key].write("游戏倒计时"+index.toString());
                        }
                        resolve(2);
                    },1000
                )
            }
        )
    }
   /* let promise=Promise.resolve();   // 此写法用于创建一个囊括于promise里面的异步
    for (let index = num; index >0; index--) {
        promise=promise.then(  // 使用promise进行延迟
            value=>{
                return new Promise(
                    resolve => {setTimeout(()=>{
                        resolve();
                        for (const key in  room_table[roomid].player) {
                            room_table[roomid].player[key].write("游戏倒计时"+index.toString());
                        }
                    },1000)}
                );
            }
        )*/
    game_start(roomid);
    room_table[roomid].game=2;
}  

function game_start(roomid:string)
{
    let max_score:number=0;
    let win_player:string[]=[];
    // 第一层循环用于寻找在游戏中的玩家
    async function player_in_game(){
        for (const key in  room_table[roomid].player) {
        let score:number=randomInt(0,2);
        if (score>max_score)  
        {
            max_score=score;
            win_player=[];   
        }
        if (score==max_score) win_player.push(key); 
        // 第二层循环对房间内的玩家进行广播
        await new Promise(
                resolve => {setTimeout(()=>{
                            for (const key2 in  room_table[roomid].player) {
                                room_table[roomid].player[key2].write("玩家"+key+"获得分数为"+score);
                            }
                            resolve(2);
                        },1000
                    )
                }
            )
        }
        let str:string='';
        if (win_player.length>1) 
        {
            str='\n未分出胜负游戏继续进行'
            game_ready(5,roomid);
        } else room_table[roomid].game==1;
        for (const key in  room_table[roomid].player) {
            room_table[roomid].player[key].write("游戏结束，最高分为："+max_score+"获胜玩家为："+win_player+str);
        }

    }
    player_in_game();
}


export const game_roll=(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>{
    let roomid=player_table[Custor.player_name].room; // 找到开启游戏玩家所在房间id
    if (typeof roomid === 'string')  
    {
        if (room_table[roomid].game==0) // 如果未准备开始游戏，需要一个计时器
        {
            room_table[roomid].game=1;
            game_ready(10,roomid);
        }   
        if (room_table[roomid].game==2)
        {
            socket.write("加入失败，游戏已经开始");
            return;
        }
        player_table[Custor.player_name].game=true;
        socket.write("成功加入游戏");
    }
    else socket.write("游戏失败，没有加入房间")
}
  