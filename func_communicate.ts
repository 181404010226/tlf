import { room_table,player_table,Room,order_judge} from "./main_listen";
import net from 'net'

// 获取对话内容的主体
function getbody(str:string):string{
    if (str.slice(0,4)=="say " ) return str.slice(0,4)
    if (str.slice(0,6)=="reply ") return str.slice(0,6)   
    return '';
}

// @功能，于外部实现，返回句中被@的所有人
function func_ait(str:string):{[key:string]:boolean}{
    let player_group:{[key:string]:boolean} ={};
    const add=(left:number,right:number)=>{
        if (!(str.slice(left,right) in player_group))   // 避免重复@
          player_group[str.slice(left,right)]=true;
    }
    for (let index = 0; index < str.length; index++) {
        if (str[index]=='@')
        {
            for (let indey = index+1; indey < str.length; indey++) {
                
                if (str[indey]=='@' || str[indey]==' ')
                {
                    console.log(index,indey);
                    add(index+1,indey);
                    break;
                }
                // 特判末尾@的情况
                if (indey+1==str.length)  add(index+1,indey+1);
            }
        }
    }
    return player_group;
}

// 某人在房间内说话，为最基本的函数,text为对话内容，roomid为对话房间，Custor为发起对话的客户端
function say_body(text:string,roomid:string,Custor:{[key:string]:any}){
    room_table[roomid].chat=room_table[roomid].chat.concat(text);
    let player_group=func_ait(text);
    console.log(player_group);
    for (const key in  room_table[roomid].player) {
        if (key!=Custor.player_name) 
        {
            if (key in player_group)
            {
                room_table[roomid].player[key].write("\n玩家:"+Custor.player_name+'@你:');
            }
            room_table[roomid].player[key].write("\n来自玩家"+Custor.player_name+'的消息:'+text);
        }
    }
}

// 对话函数
export const say_context=(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>{
    let roomid=player_table[Custor.player_name].room;
    let context:string=getbody(data.toString());
    if (typeof roomid === 'string')  
    {
        say_body(data.toString().slice(4),roomid,Custor);
    }
    else socket.write("对话失败，没有加入任何房间")
}

// 回复函数
export const reply_context=(data:Buffer,socket:net.Socket,Custor:{[key:string]:any})=>{
    let roomid=player_table[Custor.player_name].room;
    if (typeof roomid === 'string')
    {
        const func_textid=(data:Buffer):number=>{   // 获取回复的编号
            let strbody=data.toString().slice(6);
            let textid=0;
            for (let index = 0; index < strbody.length; index++) {
                if (strbody[index]>='0' && strbody[index]<='9')
                {
                    textid=textid*10+Number(strbody[index]);
                }
            }
            return textid;
        };
        let textid=func_textid(data);
        if (room_table[roomid].chat.length>textid && textid>=0)  // 数组越界判断，typescript有没有更好的判断方法？
        {
            say_body('\n回复于：'+room_table[roomid].chat[textid]+'\n'+data.toString().slice(6),roomid,Custor); // 调用对话函数
        }
        else socket.write("回复失败，此对话编号不存在")
    }
    else socket.write("回复失败，没有加入任何房间")
}