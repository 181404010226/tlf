//引入net模块

import { fdatasyncSync } from 'fs';
import net, { SocketAddress } from 'net';

//创建TCP服务器

// 创建字典
interface Player{
    name:string;
    online:boolean;
    room?:number;
}

let cnt:number=1; // 房间从1开始编号
let active_room:{[key:number]:Room}={};
interface Room{
    number:number;
    admin:string;
    active:boolean;
    player:{[key:string]:boolean};
    chat:string[];
}

let player_table:{[key:string]:Player} ={};
let room_table:{[key:string]:Room} ={};
let temp:string[]=[];


const server = net.createServer( (socket)=> {
    //  连接成功，要求玩家输入注册名字
    console.log("新玩家接入")
    let register:boolean=true; // 判断当前是否是注册账号阶段
    let name:string; // 记录本客户端所对应的玩家姓名

    //获取玩家输入的各种信息
    socket.on("data", (data)=> {
        if (register)
        {
            if (data.toString() in player_table)
            {
                socket.write('已成功登陆，账号为'+data.toString());
            }
            else 
            {
                socket.write('已成功注册，账号为'+data.toString());
            }
            player_table[data.toString()]={
                name:data.toString(),
                online:true
            };
            name=data.toString();
            register=false;
        } 
        else
        {
            let str:string=data.toString();
            if (str=='create room')
            {
                active_room[cnt]={number:cnt,
                    admin:name,
                    active:true,
                    player:{},
                    chat:[]
                };
                player_table[name].room=cnt;
                active_room[cnt].player[name]=true;
                cnt++;
                socket.write("房间创建成功，房主为"+name);
            }
            else if (str=='choose room')
            {

            }
            else if (str=='leave room')
            {

            }
      
        }
        console.log("接收到数据为：" + data.toString());
        if (data.toString()!='leave')
            socket.write("1");
    });

    socket.on("end", ()=> {
        const readSize = socket.bytesRead;
        //打印数据
        console.log("客户端已断线" );

    });

});

//设置监听端口

server.listen( 8001, () =>{

    console.log("服务正在监听中。。。")

});

