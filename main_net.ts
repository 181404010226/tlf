//引入net模块
import readline from 'readline';
import net from 'net';
import { Socket } from 'dgram';

// 一些有用的全局变量
//let player_name:string=''; // 当前客户端玩家的姓名
//创建TCP客户端
const client = new net.Socket();
//设置连接的服务器

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

client.connect(8001, '127.0.0.1',  () => {
    console.log("连接服务器成功");
    
    // ask user for the anme input
    rl.question(`请输入你的账号:`, (name) => {
      client.write(name);
    });
});


function main_order(callback:()=>void,hint?:string){
  if (hint !== undefined) console.log(hint);
  callback();
}

let client_order:{[key:string]:(callback:()=>void,hint?:string)=>void} ={};

client_order['logout']=main_order;
client_order['create room']=main_order;
client_order['choose room']=main_order;
client_order['leave room']=main_order;
client_order['say ']=main_order;
client_order['reply ']=main_order;
client_order['roll']=main_order;


//监听data事件

client.on("data",  (data) => {
  // 仅用于判断服务器连接状态，不断交替发送信号1
  if (data.toString()=='1') 
  {
    const send_order=(str:string)=>{
      client.write(str);
    }
    // 检查用户是否输入有效指令
    const check_read= ()=>{
      // question本身就作为一个异步函数
      rl.question(`请输入你的指令:`, (input) => {
        let check_str:string=input;
        if (check_str.slice(0,4)=="say " ) check_str=check_str.slice(0,4)
        if (check_str.slice(0,6)=="reply ") check_str=check_str.slice(0,6)   // 处理字符串以满足聊天指令要求
        if (check_str in client_order)
        {   // 练习回调函数
          client_order[check_str](()=>{
            send_order(input);
            if (check_str=='leave') client.end();
          });
          console.log('成功发送指令'); 
        }
        else
        {
          console.log('无效指令'); 
          check_read(); // 无效指令重复函数
        }
      });
    }
    check_read();
  }
  else if (data.toString()=='2')
  {
    rl.question(`选择房间:`, (str) => {
      client.write(str);
   });
  }
  else
  {
    console.log(data.toString());
  }
});


